//
// Created by leons on 27.12.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#include <stdio.h>
#include <stdint.h>

#pragma pack(push, 1)
struct pixel { uint8_t b, g, r; };
#pragma pack(pop)

struct image {
    uint64_t width, height;
    struct pixel* data;
};
struct pixel* getPixel(const struct image *image, uint64_t x, uint64_t y);
struct image image_create(size_t width, size_t height);
#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
