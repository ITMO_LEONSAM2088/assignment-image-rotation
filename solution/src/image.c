//
// Created by leons on 27.12.2021.
//
#include "image.h"
#include <stdlib.h>
struct image;

struct image image_create(const size_t width, const size_t height) {
    return ((struct image) {.width = width, .height = height, .data = malloc(width * height * sizeof(struct pixel))});
}
struct pixel* getPixel(const struct image *image, uint64_t x, uint64_t y) {
    return &image->data[y * image->width + x];
}
