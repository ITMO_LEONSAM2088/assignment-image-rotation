#include <BMP.h>
#include <image.h>
#include <stdio.h>
#include <stdlib.h>
#define TYPE 0x4D42
#define SIZE 40
#define PLANES 1
#define BITCOUNT 24
struct bmp_header;


enum read_status from_bmp( FILE* in, struct image* img ){


    struct bmp_header* header = malloc(sizeof(struct bmp_header));

    if (!fread(header, sizeof(struct bmp_header), 1, in)) {
        free(header);
        return READ_ERROR;
    }
    *img = image_create(header->biWidth, header->biHeight);

    for(uint32_t i=0; i< header->biHeight; i++) {
        if(!fread(&(img->data[i*img->width]), sizeof(struct pixel), header->biWidth, in)) {
            free(header);
            return READ_ERROR;
        }
        if(fseek( in, header->biWidth%4, SEEK_CUR)) {
            free(header);
            return READ_ERROR;
        }
    }






    free(header);
    if (fclose(in) == EOF) {
        return CLOSEREAD_ERROR;
    }
    return READ_OK;
}



enum write_status to_bmp( FILE* out, struct image const* img ) {

    struct bmp_header header = {
            .bfType = TYPE,
            .bfileSize = (sizeof(struct bmp_header)
                          + img->height* img->width * sizeof(struct pixel)
                          + img->height* ((img->width)%4)),
            .bOffBits = sizeof(struct bmp_header),
            .biSize = SIZE,
            .biWidth = img->width,
            .biHeight = img->height,
            .biPlanes = PLANES,
            .biBitCount = BITCOUNT,
            .biSizeImage = img->height * img->width * sizeof(struct pixel) + (img->width % 4)*img->height
    };

    if (!fwrite(&header, sizeof(struct bmp_header), 1, out)) {
        free(img->data);
        return WRITE_ERROR; }
    if(img->data!=NULL) {
        const size_t fill = 0;
        for(uint32_t h=0; h< img->height; h++) {
            if(!fwrite(&(img->data[h*img->width]),sizeof(struct pixel), img->width, out)) {
                 free(img->data);
                 return WRITE_ERROR;
            }
            if(!fwrite(&fill, 1, img->width % 4, out)) {
                 free(img->data);
                 return WRITE_ERROR;
            }
        }
    } else {
        free(img->data);
        return WRITE_ERROR;
    }
    free(img->data);
    if (fclose(out) == EOF) {
        return CLOSEWRITE_ERROR;
    }
    return WRITE_OK;
}
