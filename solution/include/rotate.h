//
// Created by leons on 27.12.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_ROTATE_H
#define ASSIGNMENT_IMAGE_ROTATION_ROTATE_H
enum  rotate_status  {
    ROTATE_OK = 0,
    ROTATE_ERROR,

};
struct image rotate( struct image const *source, struct image *output );

#endif //ASSIGNMENT_IMAGE_ROTATION_ROTATE_H
