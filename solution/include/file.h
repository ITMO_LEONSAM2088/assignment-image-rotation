//
// Created by leons on 29.01.2022.
//

#ifndef ASSIGNMENT_IMAGE_ROTATIONBOIKOSUKAEBANAT_FILE_H
#define ASSIGNMENT_IMAGE_ROTATIONBOIKOSUKAEBANAT_FILE_H
#include <stdio.h>
enum  state  {
    ALL_OK = 0,
    FILEREAD_ERROR,
    FILEWRITE_ERROR

    /* коды других ошибок  */
};
enum state rw_file(FILE **file, const char *argv, char *type);
#endif //ASSIGNMENT_IMAGE_ROTATIONBOIKOSUKAEBANAT_FILE_H

