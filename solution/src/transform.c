//
// Created by leons on 28.12.2021.
//
#include "image.h"
#include "rotate.h"

//Главное не перепутать направление
struct image transform( struct image const source, struct image (*f)(struct image const *source, struct image const *output)) {
    struct image output = {0};
    output = image_create(source.width, source.height);

    return f(&source, &output);

}


