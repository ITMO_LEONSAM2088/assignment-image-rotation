//
// Created by leons on 27.12.2021.
//
#include "image.h"
#include "rotate.h"

//Главное не перепутать направление
struct image rotate( struct image const *source, struct image *output ) {
    size_t width = output->width;
    output->width = output->height;
    output->height = width;

    for (uint64_t i = 0; i < source->width; i++) {
        for (uint64_t j = 0; j < source->height; j++) {
            struct pixel *px = getPixel(output, j, i);
            struct pixel *old_px = getPixel(source, i, (source->height-1-j));
            *px =  *old_px;
        }
    }
    return *output;

}


