#include "BMP.h"
#include "file.h"
#include "image.h"
#include "rotate.h"
#include "transform.h"
#include <stdlib.h>

int main( int argc, char** argv ) {
    (void) argc; (void) argv; // supress 'unused parameters' warning

    if (argc != 3) {
        printf("Required 3 args");

        return 1;
    }


    struct image img;
    struct image rotated_img;
    FILE *file = {0};
    FILE *new_file = {0};


    enum state state = rw_file(&file, argv[1], "rb");
    if (state != ALL_OK) {
        printf("%u", state);
        return state;
    }



    enum state state_write =  rw_file(&new_file, argv[2], "wb");
    if (state_write != ALL_OK) {
        printf("%u", state_write);
        return state_write;
    }





    const enum read_status status_code_from = from_bmp(file, &img);
    if(status_code_from!=READ_OK) {
        printf("%u", status_code_from);
        free(img.data);
        return status_code_from;
    }


    rotated_img = transform(img, rotate);


    const enum write_status status_code_to = to_bmp(new_file, &rotated_img);
    if(status_code_to!=WRITE_OK) {
        printf("%u", status_code_to);
        free(img.data);
        return status_code_to;
    }


    free(img.data);



    return 0;
}


