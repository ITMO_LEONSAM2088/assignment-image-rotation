//
// Created by leons on 28.01.2022.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION000000_TRANSFORM_H
#define ASSIGNMENT_IMAGE_ROTATION000000_TRANSFORM_H
struct image transform( struct image const source, struct image (*f)(struct image const *source, struct image *output));
#endif //ASSIGNMENT_IMAGE_ROTATION000000_TRANSFORM_H
